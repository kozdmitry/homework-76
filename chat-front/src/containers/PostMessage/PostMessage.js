import React from 'react';
import Container from "@material-ui/core/Container";
import {CssBaseline, Grid, TextField, Toolbar} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {changeInput, createMessage} from "../../store/actions/MessagesActions";
import SendIcon from '@material-ui/icons/Send';

const PostMessage = () => {
    const dispatch = useDispatch();
    const state = useSelector(state => state.messages);
    const message = useSelector(state => state.messages.message);

    const dataMessage = {
        message: state.message,
        author: state.author
    };

    const changeInfo = event => {
        dispatch(changeInput(event.target))
    };

    const post = () => {
        dispatch(createMessage(dataMessage));
    };

    return (
        <>
            <CssBaseline/>
            <Container>
                <Toolbar/>
                <Grid container direction="row" spacing={2}>
                    <Grid item>
                        <TextField
                            onChange={changeInfo}
                            name="message"
                            label="Message"
                            multiline
                            rows={2}
                            variant="outlined"
                            value={message.message}
                        >
                        </TextField>
                    </Grid>
                    <Grid item>
                        <TextField
                            onChange={changeInfo}
                            name="author"
                            label="Author"
                            variant="outlined"
                            value={message.author}
                        />
                    </Grid>
                    <Grid item>
                        <button onClick={post}><SendIcon/></button>
                    </Grid>
                </Grid>
            </Container>
        </>
    );
};

export default PostMessage;