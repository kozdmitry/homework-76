import React from 'react';
import {Paper} from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles((theme) => ({
    itemMessage: {
        border: "2px solid grey",
        margin: "10px",
        padding: "5px",
    },
}));

const ChatItem = (props) => {
    const classes = useStyles();

    return (
        <>
            <Paper className={classes.itemMessage}>
                <h4>Author: {props.author}</h4>
                <p>Message: {props.message}</p>
                <p>Date: {props.date}</p>
            </Paper>

        </>
    );
};

export default ChatItem;