const express = require('express');
const fileDb = require('../fileDb');
const router = express.Router();

router.get('/', (req, res) => {
    const messages = fileDb.getMessages();
    res.send(messages);
});


router.post('/', (req, res) => {
    if (req.body.author === "" || req.body.message === "") {
        return res.status(400).send({ error: "Author and message must be present in the request" });
    }
    fileDb.addMessage(req.body);
    res.send(req.body);
});

module.exports = router;