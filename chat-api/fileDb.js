const fs = require('fs');
const {nanoid} = require('nanoid');

const filename = './db.json';

let data = [];

module.exports = {
    init() {
        try {
            const fileContents = fs.readFileSync(filename);
            data = JSON.parse(fileContents);
        } catch (e) {
            data = [];
        }
    },
    getMessages() {
        if(data.length < 30) {
            return data;
        } else {
            const lastMessages = data.slice((data.length - 30), data.length);
            return lastMessages;
        }

    },
    addMessage(item) {
        item.id = nanoid();
        item.date = new Date().toISOString();
        data.push(item);
        this.save();
    },

    save() {
        fs.writeFileSync(filename, JSON.stringify(data, null, 2));
    }
}